import React from "react"
import { connect } from 'react-redux'
import { deletePost } from '../actions/postActions'

class Post extends React.Component {
    // now using redux to manage state instead of normal react
    // state = {
    //     post: null
    // }
    // async componentDidMount() {
    //     // console.log('>>>>>', this.props)
    //     let id = this.props.match.params.post_id
    //     try {
    //         // let res = axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
    //         let res = await axios.get(`https://jsonplaceholder.typicode.com/posts/` + id)
    //         this.setState({
    //             post: res.data
    //         })
    //     } catch (e) {
    //         console.log(e)
    //     }

    // axios.get('https://jsonplaceholder.typicode.com/posts/' + id)
    //     .then(res => {
    //         this.setState({
    //             post: res.data
    //         })
    //     })

    // }
    handleClick = () => {
        this.props.deletePost(this.props.post.id)
        this.props.history.push('/')
    }
    render() {
        console.log(this.props)
        const post = this.props.post
            ? (
                <div className="post">
                    <h3>
                        {this.props.post.title}
                    </h3>
                    <p>
                        {this.props.post.body}
                    </p>
                    <div className='center'>
                        <button className='btn red' onClick={this.handleClick}> Delete</button>
                    </div>
                </div>
            )
            : (
                <div className="center">
                    loading post ...
                </div>
            )
        return (
            <div className="container">
                <h3>
                    {post}
                </h3>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    let id = Number(ownProps.match.params.post_id)
    return {
        post: state.posts.find(post => post.id === id)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deletePost: (id) => { dispatch(deletePost(id)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Post)