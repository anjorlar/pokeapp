import React from 'react'
import Rainbow from '../hoc/Rainbow';

const About = () => {
    return (
        <div className='container'>
            <h4 className='center'> About </h4>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam egestas malesuada metus, eu eleifend massa vehicula sit amet. Etiam id lacus nibh
            </p>
        </div>
    )
}

export default Rainbow(About) //rainbow is a react higher order component