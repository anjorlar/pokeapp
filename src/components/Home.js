import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import Pokeball from '../pokeball.png'

class Home extends Component {
    // now using redux to manage state instead of normal react

    // state = {
    //     posts: []
    // }


    // async componentDidMount() {
    //     try {
    // const res = await axios.get('https://jsonplaceholder.typicode.com/photos')
    //         const res = await axios.get('https://jsonplaceholder.typicode.com/posts')
    //         this.setState({
    //             posts: res.data.slice(0, 10)
    //         })
    //         console.log(res)
    //     }
    //     catch (e) {
    //         console.log(e)
    //     }
    // }

    render() {
        // console.log(">>>>>???????", this.props)
        const { posts } = this.props
        const postList = posts.length ? (
            posts.map(post => {
                return (
                    <div className="post card" key={post.id}>
                        <img src={Pokeball} alt="pokeball" />
                        <div className='card-content'>
                            <Link to={`/${post.id}`}>
                                <span className="post-title red-text">{post.title}</span>
                            </Link>
                            <p className="post-title">{post.body}</p>
                        </div>
                    </div>
                )
            })
        ) : (
                <div className='center'>
                    No Post yet
                </div>
            )
        return (
            <div className='container home '>
                <h4 className='center'> Home </h4>
                {postList}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        posts: state.posts
    }
}

export default connect(mapStateToProps)(Home)