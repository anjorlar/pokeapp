// higher order components
import React from 'react'

const Rainbow = (WrappedComponent) => {

    const colours = ['red', 'blue', 'purple', 'orange', 'pink', 'green']
    const randomColour = colours[Math.floor(Math.random() * 5)]
    const className = `${randomColour}-text`
    // const className = randomColour + '-text'

    return (props) => {
        return (
            <div className={className}>
                <WrappedComponent{...props} />
            </div>
        )
    }
}
export default Rainbow